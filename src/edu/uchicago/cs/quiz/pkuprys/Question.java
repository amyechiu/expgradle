package edu.uchicago.cs.quiz.pkuprys;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class Question implements Serializable {

    private static final long serialVersionUID = 6546546516546843135L;

    private String country;
    private String capital;
    private String region;
    private Set<String> wrongAnswers = new HashSet<String>();

    public Question(String country, String capital, String region) {
        this.country = country;
        this.capital = capital;
        this.region = region;
    }

    public String getRegion() {
        return region;
    }

    public String getCapital() {
        return capital;
    }

    public String getCountry() {
        return country;
    }

    public Set<String> getWrongAnswers() {
        return wrongAnswers;
    }

    public boolean addWrongAnswer(String wrongAnswer){
        return wrongAnswers.add(wrongAnswer);
    }

    public String getQuestionText(){
        return "What is the capital of " + country + "?";
    }
}
